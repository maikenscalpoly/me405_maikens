''' @file lab1.py
This is mainly a testing file for lab1.  Most of the code is in motor.py.'''
import pyb
import time

from motor import MotorDriver

if __name__ == '__main__':
    # Create the pin objects used for interfacing with the motor driver
    pin_EN = pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP)
    pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4)
    pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5)
    # Create the timer object used for PWM generation
    tim = pyb.Timer(3, freq=20000)
    # Create a motor object passing in the pins and timer
    moe = MotorDriver(pin_EN, pin_IN1, pin_IN2, tim)
    # Enable the motor driver
    moe.enable()
    # Set the duty cycle to 10 percent
    moe.set_duty(10)
    time.sleep(1)
    moe.set_duty(-10)
    time.sleep(1)
    moe.disable()
    time.sleep(1)
    moe.enable()
    time.sleep(1)
    moe.disable()