## @file lab3.py
#  @page lab_3 Lab 3
#  
#  @section p_tuning P Controller Tuning
#
#  Tuning the P controller was fairly simple, I started with P = 0.8 arbitrarily,
#  which while it didn't cause much oscillation, did cause overshoot.  I divided
#  my P value in half until I didn't reach my setpoint, then I tried the value
#  between where I overshot, and where I didn't reach my setpoint and it reached
#  my setpoint almost exactly.
#
#  You can find a short video of a motor being set to encoder position 5000 at <https://www.youtube.com/watch?v=bgonpCYseM4>
#
#  Some graphs of my P tuning progress:
#  @image html run1.png "Run 1: Slight oscillation" \n
#  @image html run2.png "Run 2: Less oscillation" \n
#  @image html run3.png "Run 3: Slight overshoot" \n
#  @image html run4.png "Run 4: Not reaching set point" \n
#  @image html run5.png "Run 5: Reaching set point without overshoot almost exactly." \n

import pyb
from motor import MotorDriver
from encoder import Encoder
from controllers import Proportional, PID
import utime

def main():
    pin_EN = pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP)
    pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4)
    pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5)
    # Create the timer object used for PWM generation
    tim = pyb.Timer(3, freq=20000)
    # Create a motor object passing in the pins and timer
    moe = MotorDriver(pin_EN, pin_IN1, pin_IN2, tim)
    # Enable the motor driver
    moe.enable()
    CHA = pyb.Pin(pyb.Pin.cpu.B6, pyb.Pin.IN)
    CHB = pyb.Pin(pyb.Pin.cpu.B7, pyb.Pin.IN)
    encoder_1 = Encoder(CHA, CHB, 4)
    # we're only doing a P controller right now, not full PID
    # PD seemed to work well enough though, no need for I
    # pid_controller = PID(-15000, 1.4, 0.00, 1.291, 0)
    p_value = input("Enter a P value: ")
    p_controller = Proportional(5000, float(p_value))
    positions = []
    times = []
    iterations = 100
    while(iterations > 0):
        encoder_1.update() 
        position = encoder_1.get_position()
        motor_speed = p_controller.update(position)
        positions.append(position)
        times.append(utime.ticks_ms())
        moe.set_duty(motor_speed)
        iterations = iterations - 1
        utime.sleep_ms(10)
    print(list(zip(times, positions)))

if __name__ == "__main__":
    main()