import pyb
from motor import MotorDriver
from encoder import Encoder
import time
if __name__ == '__main__':
    pin_EN = pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP)
    pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4)
    pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5)
    # Create the timer object used for PWM generation
    tim = pyb.Timer(3, freq=20000)
    # Create a motor object passing in the pins and timer
    moe = MotorDriver(pin_EN, pin_IN1, pin_IN2, tim)
    # Enable the motor driver
    moe.enable()
    CHA = pyb.Pin(pyb.Pin.cpu.B6, pyb.Pin.IN)
    CHB = pyb.Pin(pyb.Pin.cpu.B7, pyb.Pin.IN)
    encoder_1 = Encoder(CHA, CHB, 4)
    moe.set_duty(50)
    time.sleep(5)
    moe.set_duty(-50)
    while(1):
        encoder_1.update()
        time.sleep(1)
        print(encoder_1.get_position())
