## @file project.py
#  @page project Final Project  
#  @section final_project Final Project
#
#  project.py contains python code to receive an x and y value,  each ranging 0~100
#  indicating the location of the center of a face as detected through a camera.
#
#  face_detection_opencv.py contains python code to generate the aformentioned x and y values
#  from a standard raspberry pi camera.  This is done in OpenCV using a Haar cascade, using 
#  OpenCV's pretrained model.
#
#  You can find a short video of the camera tracking my face (and, vaguely, the camera as I film)
#  here: <https://youtu.be/fACjXaQow1o>\n\n
#  
#  And here is a capture from the Pi's camera of my face, with a bounding box around it:\n
#  @image html faces_detected.jpg "Face Detection in progress" \n
#
#  Here is a screenshot of the full cad assembly:\n
#  @image html final_project_cad.png "Full assembly Screenshot" \n
#  We 3D printed the parts in PLA, which can be seen in the demo video above.
#
#  For motor control, we used a very basic P controller, with an added constant (like a PI controller,
#  but the constant was to make sure the motor would move at all, and not multiplied by error).
#  
#  One initial difficulty with i2c communications was getting the two to even detect 
#  each other - we initially tried with pull up resistors, but in the end, giving the
#  two (pi and nucleo) a common ground fixed our problems.
#
#  The code running on the raspberry pi can be found here: <https://bitbucket.org/maikenscalpoly/me405_maikens/src/master/face_detection_opencv.py>\n
#  The code running on the nucleo can be found here: <https://bitbucket.org/maikenscalpoly/me405_maikens/src/master/project.py>
#
#  The Pi ran at about 2hz at 640x480. There's probably some optimization to be done to speed things up a little, but for
#  the purposes here it worked well enough.
#
#  @author Miles Aikens and Miles Cobb
#
#  @copyright Miles Aikens and Miles Cobb 2020
#
#  @date June 11th, 2020


import pyb
from Lab1 import MotorDriver
from encoder import Encoder
import time
# Sets up i2c on i2c channel 1 as a slave on address 0x41
i2c = pyb.I2C(1, pyb.I2C.SLAVE)
i2c.init(pyb.I2C.SLAVE, addr = 0x41)
# A buffer to read data into.  Much bigger than it needs to be.
data = bytearray(200)
# Setup all the pins for motor control
pin_EN = pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP)
pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4)
pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5)
pin2_EN = pyb.Pin(pyb.Pin.cpu.C1, pyb.Pin.OUT_PP)
pin2_IN1 = pyb.Pin(pyb.Pin.cpu.A0)
pin2_IN2 = pyb.Pin(pyb.Pin.cpu.A1)
# Create the timer objects used for PWM generation
tim = pyb.Timer(3, freq=20000)
tim2 = pyb.Timer(5, freq=20000)
# Create the motor objects passing in the pins and timer
y_driver = MotorDriver(pin_EN, pin_IN1, pin_IN2, tim)
x_driver = MotorDriver(pin2_EN, pin2_IN1, pin2_IN2, tim2)
# Enable both motor drivers.
x_driver.enable()
y_driver.enable()
while True:
    try:
        data = i2c.recv(3, timeout=5000)
    except OSError:
        # This means we haven't received anything within the timeout
        # This throws an exception, but isn't an error really - just set data
        # to Nonetype.
        data = None
    if data is not None:
        # The first byte is the address, which is hardset to 0x0, so ignore it.
        x = int(data[1])
        y = int(data[2])
        print("X:", x," Y:", y)
        # essentially a P controller with a P of 0.8, and a constant of 10,
        # since 10 is the minimum that the motor needs to spin (like a constant I)
        x_driver.set_duty(((x - 50) * 0.8) + 10)
        # the Y motor does less work, and is turning "backwards" (therefore, needs a smaller constant added).
        y_driver.set_duty(((y - 50) * -0.5) + 4)

