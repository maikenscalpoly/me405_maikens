## @file motor.py
#  Brief doc for motor.py
#
#  motor.py contains only the MotorDriver class
#
#  @author Miles Aikens
#
#  @copyright Miles Aikens 2020
#
#  @date April 22nd, 2020
#
#  @package motor
#  The motor module enables easily controlling a motor attached with an L6206 driver.
#
#  This package includes a MotorDriver with an interface to allow easier control of
#  a DC motor connected to an L6206 motor driver.  It includes an interface to
#  enable/disable the motor, as well as set a positive (forward) or negative (backwards)
#  duty cycle.
#
#  @author Miles Aikens
#
#  @copyright Miles Aikens 2020
#
#  @date April 22nd, 2020
#

import pyb
import time
from math import fabs
## A motor driver object
#
#  This class allows controling the duty cycle of
#  a DC motor connected with an L6206 motor driver.
#  @author Miles Aikens
#  @copyright Miles Aikens 2020
#  @date April 22nd, 2020
class MotorDriver:
    ''' This class implements a motor driver for the
    ME405 board. '''
    def __init__ (self, EN_pin, IN1_pin, IN2_pin, timer):
        ''' Creates a motor driver by initializing GPIO
        pins and turning the motor off for safety.
        @param EN_pin A pyb.Pin object to use as the enable pin.
        @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
        @param timer A pyb.Timer object to use for PWM generation on IN1_pin
        and IN2_pin. '''
        print ('Creating a motor driver')
        self.EN_pin = EN_pin
        self.IN1_pin = IN1_pin
        self.IN2_pin = IN2_pin
        self.timer = timer
        self.forward_channel = timer.channel(1, pyb.Timer.PWM, pin=IN1_pin)
        self.backward_channel = timer.channel(2, pyb.Timer.PWM, pin=IN2_pin)
        self.disable()

    def enable (self):
        ''' This method enables the motor.  If the motor
        had a duty before being enabled, this method
        will cause the motor to start turning. '''
        print ('Enabling Motor')
        self.EN_pin.high()

    def disable (self):
        ''' This method disables the motor, but preserves
        the current duty value '''
        print ('Disabling Motor')
        self.EN_pin.low()

    def set_duty (self, duty):
        ''' This method sets the duty cycle to be sent
        to the motor to the given level. Positive values
        cause effort in one direction, negative values
        in the opposite direction.
        @param duty A signed integer holding the duty
        cycle of the PWM signal sent to the motor.  Maximum
        duty value is 100. '''
        if duty > 0:
            self.backward_channel.pulse_width_percent(0)
            # duty values greater than 100 will act like 100
            self.forward_channel.pulse_width_percent(duty)
        elif duty < 0:
            self.forward_channel.pulse_width_percent(0)
            # duty values greater than 100 will act like 100
            self.backward_channel.pulse_width_percent(fabs(duty))
        else:
            self.backward_channel.pulse_width_percent(0)
            self.forward_channel.pulse_width_percent(0)


if __name__ == '__main__':
    # Create the pin objects used for interfacing with the motor driver
    pin_EN = pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP)
    pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4)
    pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5)
    # Create the timer object used for PWM generation
    tim = pyb.Timer(3, freq=20000)
    # Create a motor object passing in the pins and timer
    moe = MotorDriver(pin_EN, pin_IN1, pin_IN2, tim)
    # Enable the motor driver
    moe.enable()
    # Set the duty cycle to 10 percent
    moe.set_duty(10)
    time.sleep(1)
    moe.set_duty(-10)
    time.sleep(1)
    moe.disable()
    time.sleep(1)
    moe.enable()
    time.sleep(1)
    moe.disable()