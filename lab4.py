## @file lab4.py
#  @page lab_4 Lab 4
#  
#  @section imu_testing IMU Testing
#
#  I tested my IMU by placing it on a protractor, then placing that protractor on 
#  another protractor and comparing the measured angle to the IMU's reported angle.
#
#  You can find a short video of the IMU being manipulated with values displayed
#  on the screen at <https://youtu.be/ljC4rItVYxs>


import pyb
import utime
imu = Imu(1, 40)
while(True):
    utime.sleep_ms(100)
    print("Euler Angle: ", imu.get_euler_tuple())
    print("Euler Velocity: ", imu.get_velocity_tuple())
    print("Calibration: ", imu.get_calibration_status())