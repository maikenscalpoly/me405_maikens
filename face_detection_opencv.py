## @file face_detection_opencv.py
#
#  face_detection_opencv.py runs a pre-trained haar cascade for face detection
#  in opencv through a Raspberry Pi camera.  This code is designed to run on a
#  Raspberry Pi connected to a STM Nucleo 64 over i2c.
#
#  @author Miles Aikens and Miles Cobb
#
#  @copyright Miles Aikens and Miles Cobb 2020
#
#  @date June 11th, 2020
#

import numpy as np
import cv2, time, os, smbus, struct
from picamera.array import PiRGBArray
from picamera import PiCamera
import time
camera = PiCamera()
camera.resolution=(640,480)
time.sleep(0.1)
rawCapture = PiRGBArray(camera)
face_cascade = cv2.CascadeClassifier('/home/pi/opencv_build/opencv/data/haarcascades/haarcascade_frontalface_default.xml')
def find_faces():
    """ Finds the faces within the camera's viewing angle, and returns the position
    of the first face as a percentage of X and Y coordinates."""
    camera.capture(rawCapture, format="bgr")
    img = cv2.flip(rawCapture.array, 0)
    img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(img_gray, 1.3, 5)
    if(len(faces) == 0):
        faces = [[img.shape[1] / 2.0, img.shape[0] / 2.0]]
    else:
        print('face found')
    x_percent = faces[0][0] * 100. / img.shape[1]
    y_percent = faces[0][1] * 100. / img.shape[0]
    rawCapture.truncate(0)
    return [int(x_percent), int(y_percent)]
# display system info
print(os.uname())
# On a raspberry pi, this corresponds with the master i2c bus
bus = smbus.SMBus(1)
# I2C address of the slave device
i2c_address = 0x41
i2c_cmd = 0x00
exit = False
while not exit:
    # really we'll never exit, since there is no user input here.  Ctrl+C will
    # exit though, which isn't ideal but will work.
    faces = find_faces()
    # print out the x and y percentages
    print(faces)
    bus.write_i2c_block_data(i2c_address, i2c_cmd, faces)