## @file encoder.py
#  Brief doc for encoder.py
#
#  encoder.py contains only the Encoder class
#
#  @author Miles Aikens
#
#  @copyright Miles Aikens 2020
#
#  @date May 4th, 2020
#
#  @package encoder
#  The encoder module enables easy usage of a quadrature encoder.
#
#  This package includes an Encoder class with an interface to allow easier control of
#  a quadrature connected to an STM-32 Nucleo 64 Dev board.  This class makes use of the
#  hardware timers on the Nucleo 64 to read values from a quadrature encoder, handling
#  overflow of the 16 bit counter and providing some useful methods.
#
#  @author Miles Aikens
#
#  @copyright Miles Aikens 2020
#
#  @date May 4th, 2020
#

import pyb

## An Encoder object
#
#  This class provides an interface to easily read from a quadrature encoder
#  using the hardware timers on an STM-32 Nucleo 64 Dev board.
#  @author Miles Aikens
#  @copyright Miles Aikens 2020
#  @date May 6th, 2020
class Encoder:

    def __init__(self, channel_a, channel_b, timer_num, timer_period=65535, timer_prescaler=0):
        """ Initializes a quadrature encoder object.
            @param channel_a The pin object to use as channel A.  Must be set to pyb.Pin.IN mode.
            @param channel_b The pin object to use as channel B.  Must be set to pyb.Pin.IN mode.
            @param timer_num The timer number to use.  Must match the passed in pin numbers.
            @param timer_period The period of the to be created timer.  Default: 65535.
            @param timer_prescaler The prescaler of the to be created timer. Default: 0. """
        self.timer = pyb.Timer(timer_num, period=timer_period, prescaler=timer_prescaler)
        self.timer.channel(1, pyb.Timer.ENC_A, pin=channel_a)
        self.timer.channel(2, pyb.Timer.ENC_B, pin=channel_b)
        # initialize positions and counts to zero
        self.position = 0
        self.previous_count = 0
        self.previous_position = 0
        # the timer period is when the timer will overflow.  In a STM-32 Nucleo 64,
        # the timer has a 16 bit counter, with a max value of 0xffff - 1, or 2^16-1= 65535
        self.max_value = timer_period
    
    def update(self):
        """ Updates the recorded position.  Must be called regularly, or the position
            may deviate from the actual position. Handles underflow/overflow 
            of the hardware timer."""
        timer_count = self.timer.counter()
        # keep track of the previous count locally, but update the object's variable
        previous_count = self.previous_count
        self.previous_count = timer_count
        # check if the hardware timer overflowed/underflowed.  If there was more than
        # one under/overflow between calls to update(self), we have no way of knowing.
        if (previous_count - timer_count) > self.max_value / 2.0:
            # the timer overflowed, handle the overflow
            timer_count = timer_count + self.max_value
        elif (previous_count - timer_count) < self.max_value / -2.0:
            # the timer underflowed, handle the underflow
            timer_count = timer_count - self.max_value
        # update our master counts for get_position() and delta()
        self.previous_position = self.position
        self.position = self.position + (timer_count - previous_count)
    
    def get_position(self):
        """ Returns the currently recorded position, as updated by update(self). """
        return self.position

    def set_position(self, new_position):
        """ Sets the current position to a value.  This will reset previous_position.
            @param new_position An integer value to store as the current position. """
        self.previous_position = new_position
        self.position = new_position

    def reset(self):
        """ Sets the current position to zero.  This will reset the previous position. """
        # we've already implemented this, just call with a constant.
        self.set_position(0)
    
    def get_delta(self):
        """ Returns the delta between the most recently recorded position and the
            previous recorded position. """
        previous_position = self.previous_position
        self.previous_position = self.position
        return self.position - previous_position

if __name__ == "__main__":
    # some testing code.  Won't get used if imported as a module.
    import time
    CHA = pyb.Pin(pyb.Pin.cpu.B6, pyb.Pin.IN)
    CHB = pyb.Pin(pyb.Pin.cpu.B7, pyb.Pin.IN)
    encoder_1 = Encoder(CHA, CHB, 4)
    CHA_2 = pyb.Pin(pyb.Pin.cpu.C6, pyb.Pin.IN)
    CHB_2 = pyb.Pin(pyb.Pin.cpu.C7, pyb.Pin.IN)
    encoder_2 = Encoder(CHA_2, CHB_2, 8)
    while(1):
        encoder_1.update()
        encoder_2.update()
        time.sleep(1)
        print(encoder_1.get_position())
        print(encoder_2.get_position())

