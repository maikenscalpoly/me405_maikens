## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#
#  @section sec_intro Introduction
#  This is a collection of documentation for labs in ME-405 at Cal Poly in Spring 2020.
#  Main code repo can be found at <https://bitbucket.org/maikenscalpoly/me405_maikens/src/master/>
#
#  @section sec_fib fib
#  The fibonacci function recursively calculates the nth fibonacci number.  This is part of lab0.py
#
#  @section sec_mot Motor Driver
#  The MotorDriver class enables easier control of a DC motor connected to an L6206 driver. Please see motor.MotorDriver which is part of the \ref motor package.
#
#  @section sec_enc Encoder
#  The Encoder class enables easy use of a Quadrature encoder. Please see encoder.Encoder which is part of the \ref encoder package.
#
#  @section sec_closed_loop Under Control
#  The controllers file contains two classes, Proportional and PID. Please see controllers.Proportional and controllers.PID which are part of the \ref controllers package.  For Lab Deliverables, see \ref lab_3
#
#  @section sec_imu Which Way is UP?
#  The IMU file contains the Imu class for use with a BNO055 imu. Please see imu.Imu is part of the \ref Imu package.  For Lab Deliverables, see \ref lab_4
#
#  @section sec_project Project Proposal
#  My project proposal is located here: \ref project_proposal. I am planning on working with Miles Cobb on this project.
#
#  @section sec_actual_project Final Project
#  My final project is located here: \ref project. I worked with Miles Cobb on this project.
#
#  @author Miles Aikens
#
#  @copyright 2020 Miles Aikens
#
#  @date April 23rd, 2020
#
