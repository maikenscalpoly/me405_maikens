''' @file lab0.py
    This is the main file for lab1
    
    This file contains fib(), and a driver for fib()
    
    @author Miles Aikens
    @copyright 2020 Miles Aikens
    @date April 22nd, 2020 '''

def fib(idx):
    ''' This method calculates a Fibonacci number corresponding to a 
        specified index.
    @param idx: An integer specifying the index of the 
                desired Fibonacci number.'''
    # these are our base cases - two are 0, and 1
    if idx <= 1:
        return idx
    else:
        # recursive case -  could speed this up by using a lookup table
        return fib(idx - 1) + fib(idx - 2)

if __name__ == '__main__':
    user_input = ""
    while True:
        user_input = input("Enter the index of the fibonacci number you would like"
            " to calculate, or type Q/Quit to quit: ")
        if str.isdigit(user_input):
            print(f"The fibonacci number at index {user_input} is {fib(int(user_input))}")
        elif user_input.lower() == "q" or user_input.lower() == "quit":
            print("Quiting...")
            exit(0)
        else:
            print("Error, you must input a non-negative integer")
