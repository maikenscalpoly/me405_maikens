## @file imu.py
#  Brief doc for imu.py
#
#  imu.py contains one class to control a BNO055 IMU over i2c
#
#  @author Miles Aikens
#
#  @copyright Miles Aikens 2020
#
#  @date May 18th, 2020
#
#  @package imu
#  The imu module includes class(es) for use with IMUs.
#
#  imu includes an Imu class with an interface to communicate with a
#  Bosch BNO055 imu over i2c.
#
#
#  @author Miles Aikens
#
#  @copyright Miles Aikens 2020
#
#  @date May 18th, 2020
#

import pyb
import ustruct

## An Imu object
#
#  This class provides an interface to communicate with a BNO055 (or compatible)
#  IMU.
#
#  @author Miles Aikens
#  @copyright Miles Aikens 2020
#  @date May 6th, 2020
class Imu:
    DEFAULT_I2C_ADDR        = 0x28
    # operation constants
    POWER_MODE_NORMAL       = 0x00
    POWER_MODE_LOWPWR       = 0x01
    POWER_MODE_SUSPEND      = 0x02
    # non-fusion mode constants
    MODE_CONFIGMODE         = 0x00
    MODE_ACCONLY            = 0x01
    MODE_MAGONLY            = 0x02
    MODE_GYRONLY            = 0x03
    MODE_ACCMAG             = 0x04
    MODE_ACCGYRO            = 0x05
    MODE_MAGGYRO            = 0x06
    MODE_AMG                = 0x07
    # fusion mode constants
    MODE_IMU                = 0x08
    MODE_COMPASS            = 0x09
    MODE_M4G                = 0x0A
    MODE_NDOF_FMC_OFF       = 0x0B
    MODE_NDOF               = 0x0C
    # axis constants
    X_AXIS_CONST            = 0x00
    Y_AXIS_CONST            = 0x01
    Z_AXIS_CONST            = 0x02
    # euler registers
    EUL_HEADING_LSB_ADDR    = 0x1A
    EUL_HEADING_MSB_ADDR    = 0x1B
    EUL_ROLL_LSB_ADDR       = 0x1C
    EUL_ROLL_MSB_ADDR       = 0x1D
    EUL_PITCH_LSB_ADDR      = 0x1E
    EUL_PITCH_MSB_ADDR      = 0x1F
    # gyroscope registers
    GYRO_X_LSB_ADDR         = 0x14
    GYRO_X_MSB_ADDR         = 0x15
    GYRO_Y_LSB_ADDR         = 0x16
    GYRO_Y_MSB_ADDR         = 0x17
    GYRO_Z_LSB_ADDR         = 0x18
    GYRO_Z_MSB_ADDR         = 0x19
    # status registers
    CALIBRATION_STATUS_ADDR = 0x35
    SELFTEST_RESULT_ADDR    = 0x36
    # mode registers
    OPERATION_MODE_ADDR     = 0x3D
    POWER_MODE_ADDR         = 0x3E

    def __init__(self, bus=1, mode=MODE_NDOF, address=DEFAULT_I2C_ADDR):
        """ Initializes a BNO055 or compatible IMU.
        @param bus (int): the i2c bus number to use for imu communications
        @param mode (int): the mode to initialize the IMU to.  Values for valid modes
                           exist in the Imu class.  Defaults to NODF mode.
        @param address (int): The i2c address for the BNO055.  Defaults to 40 (0x28)"""
        self.addr = address
        self.bus = bus
        self.i2c = pyb.I2C(bus, pyb.I2C.MASTER)
        self.i2c.mem_write(mode, self.addr, self.OPERATION_MODE_ADDR)
        # these are the defaults for degrees and DPS respectively
        self.euler_counts_per_unit = 16
        self.gyro_counts_per_unit = 16

    def enable(self):
        """ Enables the IMU by setting the power mode to normal. If the IMU
            is in config mode, this will not fully enable it - you must use set_mode()
            to enable data reporting. """
        self.i2c.mem_write(self.POWER_MODE_NORMAL, self.addr, self.POWER_MODE_ADDR)

    def disable(self):
        """ Disables the IMU by setting the power mode to SUSPEND.  """
        self.i2c.mem_write(self.POWER_MODE_SUSPEND, self.addr, self.POWER_MODE_ADDR)

    def set_mode(self, mode):
        """Sets the IMU to the specified mode.
        @param mode (int): the mode to initialize the IMU to.  Values for valid modes
                    exist in the Imu class.  Defaults to NODF mode."""
        self.i2c.mem_write(mode, self.addr, self.OPERATION_MODE_ADDR)

    def read_16bit_int(self, base_register):
        """ Helper function to read a 16bit signed integer from the passed base register. """
        return ustruct.unpack('<h', self.i2c.mem_read(2, self.addr, base_register))[0]

    def get_roll(self):
        """ Returns the current roll reported by the IMU. """
        return self.read_16bit_int(self.EUL_ROLL_LSB_ADDR) / self.euler_counts_per_unit

    def get_pitch(self):
        """ Returns the current pitch reported by the IMU. """
        return self.read_16bit_int(self.EUL_PITCH_LSB_ADDR) / self.euler_counts_per_unit

    def get_yaw(self):
        """ Returns the current yaw reported by the IMU. """
        return self.read_16bit_int(self.EUL_HEADING_LSB_ADDR) / self.euler_counts_per_unit

    def get_euler_tuple(self):
        """ Returns a tuple of roll, pitch, and yaw values."""
        return self.get_roll(), self.get_pitch(), self.get_yaw()

    def get_x_velocity(self):
        """ Returns the roll (rotation about the x axis) velocity. """
        return self.read_16bit_int(self.GYRO_X_LSB_ADDR) / self.gyro_counts_per_unit

    def get_y_velocity(self):
        """ Returns the pitch (rotation about the y axis) velocity. """
        return self.read_16bit_int(self.GYRO_Y_LSB_ADDR) / self.gyro_counts_per_unit

    def get_z_velocity(self):
        """ Returns the yaw (rotation about the Z axis) velocity """
        return self.read_16bit_int(self.GYRO_Z_LSB_ADDR) / self.gyro_counts_per_unit

    def get_velocity_tuple(self):
        """ Returns a tuple of roll, pitch, and yaw velocities. """
        return self.get_x_velocity(), self.get_y_velocity(), self.get_z_velocity()

    def get_calibration_status(self):
        """ Returns the calibration status of the system, gyroscope, accelerometer, and magnetometer
        as a tuple.  3 indicates fully calibrated, otherwise the sensor is not calibrated."""
        calibration_status = self.i2c.mem_read(1, self.addr, self.CALIBRATION_STATUS_ADDR)
        sys_status = (0xC0 & calibration_status[0]) >> 6
        gyro_status = (0x30 & calibration_status[0]) >> 4
        acc_status = (0x0C & calibration_status[0]) >> 2
        mag_status = (0x03 & calibration_status[0]) >> 0 # why not keep the pattern...
        return (sys_status, gyro_status, acc_status, mag_status)

if __name__ == '__main__':
    import pyb
    import utime
    imu = Imu(1, 40)
    while(True):
        utime.sleep_ms(100)
        print("Euler Angle: ", imu.get_euler_tuple())
        print("Euler Velocity: ", imu.get_velocity_tuple())
        print("Calibration: ", imu.get_calibration_status())