## @file controllers.py
#  Brief doc for controllers.py
#
#  controllers.py contains several classes to control systems, including a 
#  Proportional controller, as well as a PID controller.
#
#  @author Miles Aikens
#
#  @copyright Miles Aikens 2020
#
#  @date May 12th, 2020
#
#  @package controllers
#  The controllers module includes classes for closed loop control of a system.
#
#  Controllers includes a Proportional class with an interface to allow closed
#  loop control of a simple system, such as a motor, with feedback such as an encoder.
#
#  Controllers also includes a PID class with an interface to allow closed more 
#  precise closed loop control of a similar system to the Proportional controller.
#  Whereas a P controller simply uses the current error to make adjustments, a PID
#  controller uses the current error (multiplied by Kp), the sum of previous errors
#  (multiplied by Ki), and the rate of change of error (multiplied by Kd).  This 
#  can result in a much more stable system, but it will most likely require more
#  tuning, since there are three variables (Kp, Ki, and Kd) to tune.
#
#  @author Miles Aikens
#
#  @copyright Miles Aikens 2020
#
#  @date May 12th, 2020
#

import pyb

## A Proportional controller object
#
#  This class provides an easy to use P controller object for closed loop control.\n
#  @author Miles Aikens
#  @copyright Miles Aikens 2020
#  @date May 13th, 2020
class Proportional:

    def __init__(self, set_point, Kp):
        """ Initializes a quadrature encoder object.
            @param set_point (Float): Initial set point for the controller. 
            @param Kp (Float): Initial Kp value for the controller, to be multiplied by the error."""
        self.Kp = Kp
        self.set_point = set_point
    
    def update(self, current_position, set_point=None, Kp = None):
        """ Returns adjustment value for the controller.  This is calculated as
            Kp * error, where error = set_point - current_position.
            @param current_position (Float): Actualy state of the controlled system.  
            @param set_point (Float): Initial set point for the controller.
                                      Optional, uses controller's value if unspecified.
            @param Kp (Float): Initial Kp value for the controller, to be multiplied by the error.
                               Optional, uses controller's value if unspecified."""
        if set_point is None:
            set_point = self.set_point
        if Kp is None:
            Kp = self.Kp
        # return Kp * error, where error = desired - actual.
        return Kp * (set_point - current_position)
    
    def get_Kp(self):
        """ Returns the current Kp value. """
        return self.Kp

    def set_Kp(self, Kp):
        """ Sets the current Kp value to be used when update() isn't passed a Kp value.
            @param Kp (Float): Initial Kp value for the controller, to be multiplied by the error. """
        self.Kp = Kp

    def get_set_point(self):
        """ Returns the current set_point """
        self.set_point
    
    def set_set_point(self, set_point):
        """ Sets the current Kp value to be used when update() isn't passed a Kp value.
            @param set_point (Float): Initial set point for the controller. """
        self.set_point = set_point

## A PID controller object
#
#  This class provides an easy to use PID controller for precise closed-loop control
#  of a system.\n
#  @author Miles Aikens
#  @copyright Miles Aikens 2020
#  @date May 13th, 2020
class PID:

    def __init__(self, set_point, Kp, Ki, Kd, decay):
        """ Initializes a quadrature encoder object.
            @param set_point (Float): Initial set point for the controller. 
            @param Kp (Float): Initial Kp value for the controller, to be multiplied
                               by the error (desired - actual).  Increasing Kp will 
                               help the system approach the set_point faster, but may
                               lead to overshooting the setpoint.
            @param Ki (Float): Initial Ki value for the controller, to be multiplied 
                               by the total sum of error. Increasing Ki will help
                               reduce steady-state system error, but may lead to drift.
            @param Kd (Float): Initial Kd value for the controller, to be multiplied 
                               by the rate of change of the error (previous - current).
                               Increasing Kd will help the system slow down as it reaches
                               its set_point, but may slow down, or prevent you from
                               reaching your set_point.
            @param decay (Float): The amount summed error will decay with each iteration.
                                  Setting to 0 will disable decay of the steadystate error."""
        self.Kp = Kp
        self.Ki = Ki
        self.Kd = Kd
        self.decay = decay
        self.set_point = set_point
        self.previous_error = 0
        self.total_error = 0
    
    def update(self, current_position, set_point=None, Kp = None, Ki = None, Kd = None, decay=None):
        """ Returns adjustment value for the controller.  This is calculated as
            Kp * error, where error = set_point - current_position.
            @param current_position (Float): Actualy state of the controlled system.  
            @param set_point (Float): Initial set point for the controller.
                                      Optional, uses controller's value if unspecified.
            @param Kp (Float): Override Kp value for the controller, to be multiplied
                               by the error (desired - actual). Optional, uses 
                               controller's value if unspecified.
            @param Ki (Float): Override Ki value for the controller, to be multiplied 
                               by the total sum of error. Optional, uses controller's 
                               value if unspecified.
            @param Kd (Float): Override Initial Kd value for the controller, to be multiplied 
                               by the rate of change of the error (previous - current).
                               Optional, uses controller's value if unspecified."""
        if set_point is None:
            set_point = self.set_point
        if Kp is None:
            Kp = self.Kp
        if Ki is None:
            Ki = self.Ki
        if Kd is None:
            Kd = self.Kd
        if decay is None:
            decay = self.decay
        error = set_point - current_position
        self.total_error = self.total_error + error
        if(self.total_error > 0):
            self.total_error = max(0, self.total_error - decay)
        elif (self.total_error < 0):
            self.total_error = max(0, self.total_error + decay)
        return Kp * error + Ki * self.total_error + Kd * (self.previous_error - error)
    
    def get_Kp(self):
        """ Returns the current Kp value. """
        return self.Kp

    def set_Kp(self, Kp):
        """ Sets the current Kp value to be used when update() isn't passed a Kp value.
            @param Kp (Float): Kp value for the controller, to be multiplied by the error. """
        self.Kp = Kp

    def get_Ki(self):
        """ Returns the current Ki value. """
        return self.Ki

    def set_Ki(self, Ki):
        """ Sets the current Ki value to be used when update() isn't passed a Kp value.
            @param Ki (Float): @param Ki (Float): Ki value for the controller, to be multiplied 
                               by the total sum of error."""
        self.Ki = Ki

    def get_Kd(self):
        """ Returns the current Kp value. """
        return self.Kd

    def set_Kd(self, Kd):
        """ Sets the current Kp value to be used when update() isn't passed a Kp value.
            @param Kd (Float): Initial Kd value for the controller, to be multiplied 
                               by the rate of change of the error (current - previous)"""
        self.Kd = Kd

    def get_set_point(self):
        """ Returns the current set_point """
        self.set_point
    
    def set_set_point(self, set_point):
        """ Sets the current Kp value to be used when update() isn't passed a Kp value.
            @param set_point (Float): Initial set point for the controller. """
        self.set_point = set_point

if __name__ == "__main__":
    # some testing code.  Won't get used if imported as a module.
    import time


