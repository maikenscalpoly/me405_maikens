## @file proposal.py
#  Proposal for ME 405 Fianal Project Miles Cobb & Miles Aikens
#  @page project_proposal Project Proposal
#  @section problem_statement Problem Statement
#  We will enable a robot to process its surroundings and point itself at a target
#
#  We will use a closed loop control scheme with one actuator to handle the horizontal
#  panning of the robot, and another to control the vertical pivot
#  We will use encoders on these motors, as well as a camera (our new sensor) to locate our target.
#  We will be processing the camera on a RPI and sharing the data with our nucleo over SPI
#  Our system will simultaniously measure its own position while recieving image data over SPI (multi-tasking)
#
#  @section plan Collaboration Plan
#  We will use git to synchronize our work with each other.  We plan on dividing labor between the Raspberry PI 
#  camera software and the Nucleo closed-loop camera system, since it should be easy to develop those two pieces
#  of software independently, and then integrate them in the final week.
#  
#
#  @section components Additional Necesary Components:
#  - Camera (already own this)
#  - Raspberry PI (already own this)
#  - 3D printed body / parts 
#  - Hot glue gun / glue
#
#  @section manufacture Manufacture and Assembly Plan
#  1. Create body design including horizontal and vertical actuation points
#  2. 3D print pieces in PLA
#  3. Build Joints & attatch motors, RPI, and Nucleo
#
#  @section safety Safety Assessment
#  Varuious pinching may occur at the rotating joints   -   Only touch the robot when fully powered off
#  Potential melting of 3D printed parts    -   We will avoid exposing PLA to high temperatures
#  Possibly using hot glue during manufacture could burn hands  -   Use glue gun on a clean save surface
#  Don't touch the 3D printer while its hot/running.
#
#  @section timeline Timeline:
#  Week 1: Planning of structure to house components and accomodate joints \n
#  Week 2: 3D print parts, develop code on RPI to detect our target and relay its location, develop code on Nucleo to 
#          move camera to a specified position (x rotation and z rotation). \n
#  Week 3: Assemble parts and attatch sensors. Complete communication between RPI and Nucleo via SPI.  Full system 
#          integration tests, PID tuning. \n
#
#  @author Miles Cobb and Miles Aikens
#  @date May 22, 2020